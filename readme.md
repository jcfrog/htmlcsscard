# Introduction au HTML et au CSS

Ce dépot contient du matériel pour formateurs. Le but étant de faire une présentation destinée à des débutants. 

C'est une approche conceptuelle, il s'agit de faire sentir des principes, pas d'aller dans le détail des langages 
HTML et CSS, mais plutôt de comprendre comment ils fonctionnent de concert. 


# Méthodologie

## HTMLisation d'un texte

L'idée est de partir d'un simple texte structuré, en l'occurrence les premiers 
articles de la Déclaration Universelle de Droits de l'Homme (DUDH pour les intimes) 
et d'en faire pas à pas une "jolie" page WEB. Pour ce faire nous avons ouvert ce même fichier 
texte dans un éditeur, et dans le navigateur.

Au départ on montre que cela se passe très mal, le navigateur affiche un gros paquet qui a 
perdu toute structure, les caractères sont bizarres...

![Editeur et navigateur](/illustrations/edit-nav.png)

Le but sera alors d'ajouter un par un tous les éléments nécessaires pour faire 
la mise en page HTML. A chaque modification dans l'éditeur, on enregistre le 
fichier puis on le recharge dans le navigateur pour voir les effets. 
On aborde ainsi en douceur le rôle des balises, de leurs attributs, et des styles.

### Répertoire ./dudh

Les ajouts peuvent se faire à la volée mais certains étant fastifidieux, des fichiers reprennent le source à différentes étapes.

* [dudh/dudh.html](dudh/dudh.html) contient le texte de départ, sans aucun html
* [dudh/dudh-step1.html](dudh/dudh-step1.html) : ajout de `<meta charset="utf8">`, permet d'introduire les balises pour donner des informations au navigateur, montrer leur "invisibilité"
* [dudh/dudh-step2.html](dudh/dudh-step2.html) : introduction des flux de blocs, de manque de retour chariot, utilisation d'un premier bloc `<h1>`
* [dudh/dudh-step3.html](dudh/dudh-step3.html) : création d'un paragraphe et d'un lien, `<p>` et `<a>`, introduction de la notion d'attribut de balise
* [dudh/dudh-step4.html](dudh/dudh-step4.html) : structuration paragraphes, sous titres `<p>`, `<h2>` 
* [dudh/dudh-step5.html](dudh/dudh-step5.html) : structuration liste ordonnée grâce à l'article 2 qui comporte 2 puces , `<ol>/<li>`
* [dudh/dudh-step6.html](dudh/dudh-step6.html) : ajout de la structure englobante de la page `<DOCTYPE>, <html>, <head>, <title>, <body>`
* [dudh/dudh-step7.html](dudh/dudh-step7.html) : ajout d'une image `<img>`
* [dudh/dudh-step8.html](dudh/dudh-step8.html) : introduction `<style>` et premières règles CSS
* [dudh/dudh-step9.html](dudh/dudh-step9.html) : introduction de id et classes pour ciblage balises/éléments
* [dudh/dudh-step10.html](dudh/dudh-step10.html) : création d'un fichier CSS monstyle.css et introduction au lien `<link rel="stylesheet" type="text/css" href= « »>`

Au final on arrive à une page WEB valide et "joliment" mise en page :)

![Editeur et navigateur](/illustrations/edit-nav-final.png)

## Etude de style

Un deuxième exemple est proposé pour insister sur une notion de base : 
* le HTML décrit le contenu et sa structure ( titres, paragraphes, listes à puces, liens, images, ... )
* le CSS vient mettre en forme ces éléments ( couleur, fond, marges, taille ...) grâce à un ciblage précis ( balises, identifiants, classes )

Nous avons donc une page WEB d'invitation à un atelier avec des id, des classes, des styles. 

### Répertoire ./invitation

Ce répertoire contient 

* le fichier HTML [invitation/invitation.html](invitation/invitation.html)
* le style [invitation/css/mapresentation.css](invitation/css/mapresentation.css)
* une image [invitation/img/codefondsmall.jpg](invitation/img/codefondsmall.jpg)

Le répertoire style contient également un CSS de reset.

Etapes pédagogiques : 

* On montre la page, puis la même si on met tout le style en commentaire, pour bien illustrer que "tout" le style est fait par le CSS.
* On décommente petit à petit les styles pour voir les effets, ainsi que la façon dont les éléments sont affectés, "ciblés".


# Licence

CC0

# Commentaire 

Ce cours a été présenté pour la 1ere fois le [10 avril 2018](http://adn56.net/blog/index.php/2018/04/12/cr-html-css/) dans le cadre 
d'éducation populaire au numérique à Damgan, avec un public débutant varié ( de ado à retraité ), et ça s'est très bien passé :)